const initStateRegister = {
    name: '',
    email: '',
    password: '',
    passwword_confirmation: '',
}

export const registerReducer = (state = initStateRegister, action) => {
    if(action.type == 'SET_REGISTER'){
        return {
            ...state,
            name: action.value.name,
            email: action.value.email,
            password: action.value.password,
            passwword_confirmation: action.value.password,
        }
    }
    return state;
}