import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

const Button = ({label, color='#FF6B6B', onPress}) => {
  return (
    <TouchableOpacity style={styles.container(color)} activeOpacity={0.7} onPress={onPress}>
      <Text style={styles.text}>{label}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
    container: (color)=> ({
        borderRadius: 10,
        padding: 12,
        backgroundColor: color,
    }),
    text: {
        color: 'white',
        fontFamily: 'Poppins-Medium',
        fontSize: 14,
        textAlign: 'center'
    }
});
