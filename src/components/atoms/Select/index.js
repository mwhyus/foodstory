import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Picker } from "@react-native-picker/picker";

const Select = ({label, value, onSelectChange}) => {

    return (
        <View>
            <Text style={styles.label}>{label}</Text>
            <View style={styles.container}>
                <Picker
                    selectedValue={value}
                    onValueChange={(itemValue) =>
                        onSelectChange(itemValue)
                    }>
                    <Picker.Item label="Jakarta" value="Jakarta" />
                    <Picker.Item label="Bandung" value="Bandung" />
                    <Picker.Item label="Makassar" value="Makassar" />
                    <Picker.Item label="Jogjakarta" value="Jogjakarta" />
                    <Picker.Item label="Semarang" value="Semarang" />

                </Picker>
            </View>
        </View>
    )
}

export default Select

const styles = StyleSheet.create({
    label: {
        fontSize: 16,
        fontFamily: 'Poppins-Regular',
        color: '#020202'
    },
    container: {
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#8D92A3',
        paddingHorizontal: 2,
        paddingVertical: 0
    }
})
