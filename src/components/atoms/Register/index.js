import React from 'react';
import { StyleSheet, Text, TouchableOpacity, } from 'react-native';

const Register = ({onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress} >
      <Text style={styles.text}>Create a new account</Text>
    </TouchableOpacity>
  );
};

export default Register;

const styles = StyleSheet.create({
    container: {alignSelf: 'center'},
    text: {color: '#8D92A3'}
});
