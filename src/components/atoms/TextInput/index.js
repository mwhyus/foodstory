import { StyleSheet, Text, View, TextInput } from 'react-native';
import React from 'react';

const Input = ({label, placeholder, ...restProps}) => {
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
        <TextInput style={styles.container} placeholder={placeholder} {...restProps} />
    </View>
  );
};

export default Input;

const styles = StyleSheet.create({
    label: {
        fontSize: 16,
        fontFamily: 'Poppins-Regular',
        color: '#020202'
    },
    container: {
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#8D92A3',
        padding: 10
    }
});
