import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import { ICBack } from '../../../assets/icon';

const Header = ({title, subtitle, onPress, onBack}) => {
  return (
    <View style={styles.container}>
      {
        onBack && (
          <TouchableOpacity onPress={onPress} activeOpacity={0.7} style={styles.back}>
            <ICBack />
          </TouchableOpacity>        
        )
      }
        <View>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subTitle}>{subtitle}</Text>
        </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 24,
        paddingTop: 30,
        paddingBottom: 24,
        flexDirection: 'row',
        alignItems: 'center'
    },
    back: {
      padding: 16,
      marginRight: 16,
      marginLeft: -10
    },
    title: {
        fontSize: 24,
        fontFamily: 'Poppins-Medium',
        color: '#020202'
    },
    subTitle: {
        fontSize: 14,
        fontFamily: 'Poppins-Light',
        color: '#8D92A3'
    }
});
