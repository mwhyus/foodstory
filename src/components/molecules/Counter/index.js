import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, {useState} from 'react'
import { Gap } from '../../atoms'
import { ICMinus, ICPlus } from '../../../assets'

const Counter = () => {

    const [count, setCount] = useState(0)
    return (
        <View style={styles.container}>
            <TouchableOpacity  onPress={() => setCount(count => Math.max(count - 1, 0))} >
                <ICMinus />
            </TouchableOpacity>
            <Gap width={16} />
            <Text style={styles.number}>{count}</Text>
            <Gap width={16} />
            <TouchableOpacity>
                <ICPlus onPress={() => setCount(count + 1)} />
            </TouchableOpacity>
        </View>
    )
}

export default Counter

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    number: {
        fontSize: 20,
    },
})