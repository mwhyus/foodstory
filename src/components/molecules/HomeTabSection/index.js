import * as React from 'react';
import { StyleSheet, Text, useWindowDimensions, View, Image, ScrollView } from 'react-native'
import { SceneMap, TabView, TabBar } from 'react-native-tab-view';
import { ItemListFood, Rating } from '..';
import { DMFood5 } from '../../../assets';
import { useNavigation } from '@react-navigation/native';


const NewTaste = () => {
    const navigation = useNavigation();
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ paddingTop: 8, paddingHorizontal: 24 }}>
                <ItemListFood
                    type='product'
                    image={DMFood5}
                    title='Soup'
                    rating price='100.000'
                    onPress={() => navigation.navigate('FoodDetail')}
                />
                <ItemListFood
                    type='product'
                    image={DMFood5}
                    title='Soup'
                    rating price='100.000'
                    onPress={() => navigation.navigate('FoodDetail')}
                />
                <ItemListFood
                    type='product'
                    image={DMFood5}
                    title='Soup'
                    rating price='100.000'
                    onPress={() => navigation.navigate('FoodDetail')}
                />
            </View>
        </ScrollView>
    )
};

const Popular = () => {
    const navigation = useNavigation();
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ paddingTop: 8, paddingHorizontal: 24 }}>
                <ItemListFood type='product' image={DMFood5} price='100.000' rating onPress={() => navigation.navigate('FoodDetail')} />
                <ItemListFood type='product' image={DMFood5} price='100.000' rating onPress={() => navigation.navigate('FoodDetail')} />
                <ItemListFood type='product' image={DMFood5} price='100.000' rating onPress={() => navigation.navigate('FoodDetail')} />
            </View>
        </ScrollView>
    )
};

const Recommended = () => {
    const navigation = useNavigation();
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ paddingTop: 8, paddingHorizontal: 24 }}>
                <ItemListFood type='product' image={DMFood5} price='100.000' rating onPress={() => navigation.navigate('FoodDetail')} />
                <ItemListFood type='product' image={DMFood5} price='100.000' rating onPress={() => navigation.navigate('FoodDetail')} />
                <ItemListFood type='product' image={DMFood5} price='100.000' rating onPress={() => navigation.navigate('FoodDetail')} />
            </View>
        </ScrollView>
    )
};

const renderScene = SceneMap({
    1: NewTaste,
    2: Popular,
    3: Recommended
});

const renderTabBar = props => (
    <TabBar
        {...props}
        indicatorStyle={{ backgroundColor: '#020202', height: 3 }}
        style={{ backgroundColor: 'white' }}
        tabStyle={{ width: 'auto' }}
        renderLabel={({ route, focused, color }) => (
            <Text style={{ fontFamily: 'Poppins-Medium', color: focused ? '#020202' : '#8D92A3' }}>
                {route.title}
            </Text>
        )}
    />
);

const HomeTabSection = () => {

    const layout = useWindowDimensions();

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: '1', title: 'New Taste' },
        { key: '2', title: 'Popular' },
        { key: '3', title: 'Recommended' },
    ]);
    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
            renderTabBar={renderTabBar}
            style={{ backgroundColor: 'white' }}
        />
    )
}

export default HomeTabSection

const styles = StyleSheet.create({})