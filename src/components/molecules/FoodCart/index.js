import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Rating } from '..';

const FoodCart = ({ image, name, onPress }) => {
  return (
    <TouchableOpacity style={styles.container} activeOpacity={0.7} onPress={onPress} >
      <Image source={image} style={styles.food} />
      <View style={styles.foodInfo} >
        <Text style={styles.text}>{name}</Text>
        <Rating />
      </View>
    </TouchableOpacity>
  );
};

export default FoodCart;

const styles = StyleSheet.create({
  container: {
    width: 180,
    height: 270,
    backgroundColor: 'white',
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 7 },
    shadowOpacity: 0.5,
    shadowRadius: 10,
    elevation: 14,
    overflow: 'hidden',
    marginRight: 24
  },
  foodInfo: {
    padding: 8
  },
  food: {
    width: 200,
    height: 190,
  },
  text: {
    fontSize: 14,
    color: '#020202',
    fontFamily: 'Poppins-Regular',
  },


});
