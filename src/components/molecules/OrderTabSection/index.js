import * as React from 'react';
import { StyleSheet, Text, useWindowDimensions, View, Image, ScrollView } from 'react-native'
import { SceneMap, TabView, TabBar } from 'react-native-tab-view';
import { ItemListFood, Rating } from '..';
import { DMFood5 } from '../../../assets';
import { useNavigation } from '@react-navigation/native';


const InProgress = () => {
    const navigation = useNavigation();
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ paddingTop: 8, paddingHorizontal: 24 }}>
                <ItemListFood
                    image={DMFood5}
                    title='Soup'
                    rating price='100.000'
                    onPress={() => navigation.navigate('OrderDetail')}
                    type='in-progress'
                    items={3}   
                    price='100.000'
                />
                <ItemListFood
                    image={DMFood5}
                    title='Soup'
                    rating price='100.000'
                    onPress={() => navigation.navigate('OrderDetail')}
                    type='in-progress'
                    items={3}
                    price='100.000'
                />
                <ItemListFood
                    image={DMFood5}
                    title='Soup'
                    rating price='100.000'
                    onPress={() => navigation.navigate('OrderDetail')}
                    type='in-progress'
                    items={3}
                    price='100.000'
                />
            </View>
        </ScrollView>
    )
};

const HistoryOrder = () => {
    const navigation = useNavigation();
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ paddingTop: 8, paddingHorizontal: 24 }}>
            <ItemListFood
                    image={DMFood5}
                    rating
                    onPress={() => navigation.navigate('OrderDetail')}
                    type='history-order'
                    title='Soup'
                    items={3}
                    price='1.000.000'
                    date='17/02/2022'
                />
                <ItemListFood
                    image={DMFood5}
                    rating
                    onPress={() => navigation.navigate('OrderDetail')}
                    type='history-order'
                    title='Soup'
                    items={3}
                    price='1.000.000'
                    date='17/02/2022'
                    status='Delivered'
                />
            </View>
        </ScrollView>
    )
};

const renderScene = SceneMap({
    1: InProgress,
    2: HistoryOrder,
});

const renderTabBar = props => (
    <TabBar
        {...props}
        indicatorStyle={{ backgroundColor: '#020202', height: 3 }}
        style={{ backgroundColor: 'white' }}
        tabStyle={{ width: 'auto' }}
        renderLabel={({ route, focused, color }) => (
            <Text style={{ fontFamily: 'Poppins-Medium', color: focused ? '#020202' : '#8D92A3' }}>
                {route.title}
            </Text>
        )}
    />
);

const OrderTabSection = () => {

    const layout = useWindowDimensions();

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: '1', title: 'In Progress' },
        { key: '2', title: 'History Order' },
    ]);
    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
            renderTabBar={renderTabBar}
            style={{ backgroundColor: 'white' }}
        />
    )
}

export default OrderTabSection

const styles = StyleSheet.create({})