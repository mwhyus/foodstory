import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { ICStarOff, ICStarOn } from '../../../assets'

const Rating = () => {
    return (
        <View style={styles.ratingContainer}>
            <View style={styles.starContainer}>
                <ICStarOn />
                <ICStarOn />
                <ICStarOn />
                <ICStarOn />
                <ICStarOff />
                <Text>4.5</Text>
            </View>
        </View>


    )
}

export default Rating

const styles = StyleSheet.create({
    ratingContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
      },
    starContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
})