import { StyleSheet, Text, View, Image } from 'react-native';
import React from 'react';
import { DMPerson } from '../../../assets';

const HeaderHome = () => {
  return (
    <View style={styles.container}>
        <View>
            <Text style={styles.title}>FoodStory</Text>
            <Text style={styles.subTitle}>Every food has their</Text>
            <Text style={styles.subTitle}>own story</Text>
        </View>
        <Image source={DMPerson} style={styles.profile} />
    </View>
  );
};

export default HeaderHome;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 24,
        paddingTop: 20,
        paddingBottom: 10,
        backgroundColor: 'white',
        // alignItems: 'center'
    },
    title: {
        fontSize: 22,
        color: '#020202',
        fontFamily: 'Poppins-Medium'
    },
    subTitle: {
        fontSize: 14,
        color: '#8D92A3',
        fontFamily: 'Poppins-Light'
    },
    profile: {
        width: 50,
        height: 50,
        borderRadius: 8
    }
});
