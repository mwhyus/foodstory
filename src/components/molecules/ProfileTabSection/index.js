import * as React from 'react';
import { StyleSheet, Text, useWindowDimensions, View, Image, ScrollView } from 'react-native'
import { SceneMap, TabView, TabBar } from 'react-native-tab-view';
import { ItemListFood, Rating } from '..';
import { DMFood5 } from '../../../assets';
import { useNavigation } from '@react-navigation/native';
import ItemListMenu from '../ItemListMenu';


const Account = () => {
    const navigation = useNavigation();
    return (
        <View style={{ paddingTop: 8, paddingHorizontal: 24 }}>
            <ItemListMenu text='Edit Profile' />
            <ItemListMenu text='Home Address' />
            <ItemListMenu text='Security' />
            <ItemListMenu text='Payments' />
        </View>
    )
};

const FoodStory = () => {
    const navigation = useNavigation();
    return (
        <View style={{ paddingTop: 8, paddingHorizontal: 24 }}>
            <ItemListMenu text='Rate App' />
            <ItemListMenu text='Help Center' />
            <ItemListMenu text='Privacy & Policy' />
            <ItemListMenu text='Terms & Conditions' />
        </View>
    )
};

const renderScene = SceneMap({
    1: Account,
    2: FoodStory,
});

const renderTabBar = props => (
    <TabBar
        {...props}
        indicatorStyle={{ backgroundColor: '#020202', height: 3 }}
        style={{ backgroundColor: 'white' }}
        tabStyle={{ width: 'auto' }}
        renderLabel={({ route, focused, color }) => (
            <Text style={{ fontFamily: 'Poppins-Medium', color: focused ? '#020202' : '#8D92A3' }}>
                {route.title}
            </Text>
        )}
    />
);

const ProfileTabSection = () => {

    const layout = useWindowDimensions();

    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
        { key: '1', title: 'Account' },
        { key: '2', title: 'FoodStory' },
    ]);
    return (
        <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={{ width: layout.width }}
            renderTabBar={renderTabBar}
            style={{ backgroundColor: 'white' }}
        />
    )
}

export default ProfileTabSection

const styles = StyleSheet.create({})