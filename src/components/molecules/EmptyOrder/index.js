import LottieView from 'lottie-react-native';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Gap } from '../..';
import { ANLookingForFood } from '../../../assets';
import { useNavigation } from '@react-navigation/native';


const EmptyOrder = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.page}>
            <LottieView source={ANLookingForFood} autoPlay loop style={styles.lottieview} />
            <View style={styles.container}>
                <Text style={styles.title}>No Food</Text>
                <Gap height={6} />
                <Text style={styles.subTitle}>Seems like you have'nt</Text>
                <Text style={styles.subTitle}>ordered food today</Text>
                <Gap height={30} />
                <View style={styles.buttonContainer}>
                    <Button
                        label='Find foods'
                        onPress={() => navigation.replace('MainApp', screen='MainApp')}
                    />
                </View>
            </View>
        </View>
    )
}

export default EmptyOrder

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white'
    },

    lottieview: {
        height: '55%',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    container: {
        height: '45%',
        justifyContent: 'flex-start',
        alignItems: 'center',

    },
    ilustration: {
        height: 250,
        width: 250,

    },
    title: {
        fontSize: 20,
        fontFamily: 'Poppins-Regular',
        color: '#020202'
    },
    subTitle: {
        fontSize: 14,
        fontFamily: 'Poppins-Light',
        color: '#8D92A3'
    },
    buttonContainer: {
        width: '100%',
        paddingHorizontal: 80
    }
})
