import { NavigationContainer } from '@react-navigation/native';
import * as React from 'react';
import { LogBox } from 'react-native';
import Router from './router';
import { Provider } from 'react-redux';
import store from './redux/store';

const App = () => {
  LogBox.ignoreAllLogs();
  return (
    <NavigationContainer>
      <Provider store={store}>
        <Router />
      </Provider>
    </NavigationContainer>
  );
};

export default App;
