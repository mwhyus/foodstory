import * as React from 'react';
import { ScrollView, StyleSheet, View, SafeAreaView } from 'react-native';
import { DMFood1, DMFood2, DMFood3 } from '../../assets';
import { FoodCart, Gap, HeaderHome, HomeTabSection } from '../../components';



const Home = ({navigation}) => {

    return (
        <SafeAreaView style={styles.page}>
            <HeaderHome />
            <Gap height={22} />
            <View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false} >
                    <View style={styles.foodCart}>
                        <FoodCart onPress={() => navigation.navigate('FoodDetail')} image={DMFood1} name='Chicken and Cauliflower' />
                        <FoodCart onPress={() => navigation.navigate('FoodDetail')} image={DMFood2} name='Beef tanderloin and balsamic steak' />
                        <FoodCart onPress={() => navigation.navigate('FoodDetail')} image={DMFood3} name='Cranberry-Drizzled' />
                    </View>
                </ScrollView>
            </View>
            <Gap height={22} />
            <View style={styles.tabContainer}>
                <HomeTabSection />
            </View>
        </SafeAreaView>
    )
}

export default Home

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    tabContainer: {
        flex: 1
    },
    foodCart: {
        flexDirection: 'row',
        paddingLeft: 22,
        backgroundColor: 'white',
    }
})