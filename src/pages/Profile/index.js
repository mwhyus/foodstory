import React from 'react'
import { Image, StyleSheet, Text, View, SafeAreaView } from 'react-native'
import { DMPerson } from '../../assets'
import { Button, Gap, ProfileTabSection } from '../../components'

const Profile = ({navigation}) => {
    return (
        <SafeAreaView style={styles.page}>
            <View style={styles.profileDetail}>
                <View style={styles.photo}>
                    <View style={styles.borderPhoto}>
                        <Image source={DMPerson} style={styles.photoContainer} />
                    </View>
                </View>
                <Text style={styles.name}>Mikasa Ackerman</Text>
                <Text style={styles.email}>mikasaackerman@gmail.com</Text>
            </View>
            <Gap height={24} />
            <View style={styles.content}>
                <ProfileTabSection />
            </View>

            <View style={styles.signout}>
                <Button label='Sign Out' onPress={() => navigation.navigate('Login')} />
            <Gap height={24} />
            </View>
        </SafeAreaView>
    )
}

export default Profile

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    profileDetail: {
        backgroundColor: 'white',
        paddingBottom: 26
    },
    photo: {
        alignItems: 'center',
        marginTop: 26,
        marginBottom: 16,
    },
    borderPhoto: {
        borderWidth: 1,
        borderColor: '#8D92A3',
        width: 110,
        height: 110,
        borderRadius: 110,
        borderStyle: 'dashed',
        justifyContent: 'center',
        alignItems: 'center'
    },
    photoContainer: {
        width: 90,
        height: 90,
        borderRadius: 90,
        backgroundColor: '#F0F0F0',
        padding: 24,
    },
    content: {
        flex: 1,
    },
    name: {
        fontSize: 18,
        fontFamily: 'Poppins-Medium',
        color: '#020202',
        textAlign: 'center',
    },
    email: {
        fontSize: 13,
        fontFamily: 'Poppins-Regular',
        color: '#8D92A3',
        textAlign: 'center',
    },
    signout: {
        paddingHorizontal: 24,
        backgroundColor: 'white'
    }
})
