import { Alert, ScrollView, StyleSheet, Text, View, SafeAreaView } from 'react-native';
import React, { useState } from 'react';
import { Button, Gap, Header, Input, Register } from '../../components';
import {useForm} from '../../utils';
import Axios from 'axios';
import { API_URL } from '../../utils/API';


const Login = ({ navigation }) => {
  // const [email, setEmail] = useState('')
  // const [password, setPassword] = useState('')

  const [form, setForm] = useForm({
    email: '',
    password: ''
  })

  const onSubmit = () => {
    Axios.post(`${API_URL}/api/login`, form)
    .then(res => {
      Alert.alert('Login Berhasil')
      console.log('Responnya: ', res);
      // navigation.navigate('MainApp')
    })
    .catch(err => {
      // Alert.alert(err)
      console.log('Errornya: ', err);
    })
  }

  return (
    <SafeAreaView style={styles.bigContainer}>
      <Header onPress={() => navigation.goBack()} title='Sign In' subtitle='Continue to find a best food' />
      <View style={styles.container}>
        <Input
          label='Email'
          placeholder='Enter your email'
          value={form.email}
          onChangeText={(value) => setForm('email', value)}
        />
        <Gap height={16} />
        <Input 
          label='Password' 
          placeholder='Enter your password'
          value={form.password}
          onChangeText={(value) => setForm('password', value)}
          secureTextEntry
        />
        <Gap height={130} />
        <Button label='Sign In' onPress={onSubmit} />
        <Gap height={10} />
        <Register onPress={() => navigation.navigate('Register')} />
      </View>
    </SafeAreaView>
  );
};

export default Login;

const styles = StyleSheet.create({
  bigContainer: {
    flex: 1,
  },
  container: {
    backgroundColor: 'white',
    paddingTop: 26,
    paddingHorizontal: 24,
    marginTop: 24,
    flex: 1
  }
});
