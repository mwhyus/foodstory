import LottieView from 'lottie-react-native';
import React from 'react';
import { StyleSheet, Text, View, SafeAreaView } from 'react-native';
import { ANsuccess2 } from '../../assets';
import { Button, Gap } from '../../components';



const OrderSuccess = ({ navigation }) => {

    return (
        <SafeAreaView style={styles.page}>
            <View style={styles.lottieview}>
                <LottieView source={ANsuccess2} autoPlay loop  />
            </View>
            <View style={styles.container}>
                <Text style={styles.title}>You found your story!</Text>
                <Gap height={6} />
                <Text style={styles.subTitle}>Just chil and relax, we are </Text>
                <Text style={styles.subTitle}>making magic on your story.</Text>
                <Gap height={30} />
                <View style={styles.buttonContainer}>
                    <Button label='Order Again' onPress={() => navigation.replace('MainApp')} />
                    <Gap height={12} />
                    <Button
                        label='View My Order'
                        color='#8D92A3'
                        onPress={() => navigation.replace('MainApp', { screen: 'Order' })}
                    />
                </View>
            </View>
        </SafeAreaView>
    )
}

export default OrderSuccess

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white'

    },

    lottieview: {
        height: '55%',
        justifyContent: 'flex-end',
    },

    container: {
        height: '45%',
        justifyContent: 'flex-start',
        alignItems: 'center',

    },
    ilustration: {
        height: 250,
        width: 250,

    },
    title: {
        fontSize: 20,
        fontFamily: 'Poppins-Regular',
        color: '#020202'
    },
    subTitle: {
        fontSize: 14,
        fontFamily: 'Poppins-Light',
        color: '#8D92A3'
    },
    buttonContainer: {
        width: '100%',
        paddingHorizontal: 80
    }
})