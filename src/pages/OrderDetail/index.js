import { Alert, ScrollView, StyleSheet, Text, View, SafeAreaView } from 'react-native'
import React from 'react'
import { Button, Gap, Header, ItemListFood, ItemValue } from '../../components'
import { DMFood1 } from '../../assets'

const OrderDetail = ({navigation}) => {
    return (
        <SafeAreaView>
        <ScrollView showsVerticalScrollIndicator={false} >
            <Header title='Payment' subtitle='Check your order' onBack onPress={() => navigation.goBack()} />
            <View>
                <Gap height={24} />
                <View style={styles.content}>
                    <Text>Item Ordered</Text>
                    <ItemListFood type='order-summary' image={DMFood1} items={3} title='Chicken and Couliflower' price={120000} />
                    <Text style={styles.label}>Details Transaction</Text>
                    <ItemValue label='Chicken and Couliflower' value={360000} />
                    <ItemValue label='Driver' value='IDR 50.000' />
                    <ItemValue label='Tax 10%' value='IDR 20.000' />
                    <ItemValue label='Grand Total' value='IDR 430.000' valueColor='#1ABC9C' />
                </View>
                <Gap height={24} />
                <View style={styles.content}>
                    <Text style={styles.label}>Deliver to:</Text>
                    <ItemValue label='Name' value='Saske' />
                    <ItemValue label='Phone No.' value='08123456780' />
                    <ItemValue label='Address' value='Menteng' />
                    <ItemValue label='House No.' value='A20-21' />
                    <ItemValue label='City' value='Jakarta Pusat' />
                </View>
                <Gap height={24} />
                <View style={styles.content}>
                    <Text style={styles.label}>Order Status:</Text>
                    <ItemValue label='#FE2RFDWEE1' value='Paid' valueColor='#1ABC9C' />
                </View>
            </View>
            <Gap height={18} />
            <View style={styles.checkout}>
                <Button label='Cancel Order' color='#D9435E' onPress={() => Alert.alert('Really?')} />
            </View>
            <Gap height={20} />
        </ScrollView>
        </SafeAreaView>
    )
}

export default OrderDetail

const styles = StyleSheet.create({
    content: {
        backgroundColor: 'white',
        paddingHorizontal: 24,
        paddingVertical: 16,
    },
    label: {
        fontSize: 14,
        fontFamily: 'Poppins-Regular',
        color: '#020202',
        marginBottom: 8
    },
    checkout: {
        paddingHorizontal: 24,
        
    }
})