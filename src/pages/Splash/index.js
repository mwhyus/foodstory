import React, {useEffect} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ICLogo } from '../../assets/icon';
import LottieView from 'lottie-react-native';
import { ANFoodStory } from '../../assets';


const Splash = ({navigation}) => {

  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Login')
    }, 3000)
  }, [])

  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <View style={styles.logo}>
          <ICLogo />
        </View>
        <Text style={styles.Text}>FoodStory</Text>
      </View>
      <LottieView source={ANFoodStory} autoPlay loop />
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FF6B6B",
  },
  logoContainer: {
    padding: 10
  },
  logo: {
    marginLeft: 12
  },
  Text: {
    fontSize: 18,
    fontFamily: 'Poppins-Medium'
  }
});
