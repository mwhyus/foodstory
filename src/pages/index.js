import Splash from "./Splash"
import Login from './Login'
import Register from './Register'
import Address from './Address'
import SuccessRegister from "./SuccessRegister";
import Home from "./Home";
import Order from './Order'
import Profile from './Profile'
import FoodDetail from "./FoodDetail";
import OrderSummary from "./OrderSummary";
import OrderSuccess from "./OrderSuccess";
import OrderDetail from "./OrderDetail";

export {
    Splash,
    Login,
    Register,
    Address,
    SuccessRegister,
    Home,
    Order,
    Profile,
    FoodDetail,
    OrderSummary,
    OrderSuccess,
    OrderDetail
}