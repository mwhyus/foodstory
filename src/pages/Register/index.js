import React from 'react'
import { ScrollView, StyleSheet, Text, View, SafeAreaView } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { Button, Gap, Header, Input } from '../../components'
import { useForm } from '../../utils';


const Register = ({ navigation }) => {
    const [form, setForm] = useForm({
        name: '',
        email: '',
        password: '',
    })

    const dispatch = useDispatch()
    const onSubmit = () => {
        console.log('Formnya: ', form);
        dispatch({type: 'SET_REGISTER', value: form})
        navigation.navigate('Address')
    }


    return (
        <SafeAreaView>
        <ScrollView contentContainerStyle={{flexGrow: 1}} >
            <View style={styles.bigContainer}>
                <Header
                    onPress={() => navigation.goBack()}
                    title='Register'
                    subtitle="Start your journey"
                    onBack
                />
                <View style={styles.container}>
                    <View style={styles.photo}>
                        <View style={styles.borderPhoto}>
                            <View style={styles.photoContainer}>
                                <Text style={styles.addPhoto}>Add Photo</Text>
                            </View>
                        </View>
                    </View>
                    <Input
                        label='Full Name'
                        placeholder='Enter your full name'
                        value={form.name}
                        onChangeText={(value) => setForm('name', value)}
                    />
                    <Gap height={16} />
                    <Input
                        label='Email'
                        placeholder='Enter your email'
                        value={form.email}
                        onChangeText={(value) => setForm('email', value)}
                    />
                    <Gap height={16} />
                    <Input
                        label='Password'
                        placeholder='Enter your password'
                        value={form.password}
                        onChangeText={(value) => setForm('password', value)}
                        secureTextEntry
                    />
                    <Gap height={26} />
                    <Button label='Continue' onPress={onSubmit} />
                    <Gap height={16} />
                </View>
            </View>
        </ScrollView>
        </SafeAreaView>
    )
}

export default Register

const styles = StyleSheet.create({
    bigContainer: {
        flex: 1,
    },
    container: {
        backgroundColor: 'white',
        // paddingTop: 20,
        paddingHorizontal: 24,
        marginTop: 24,
        flex: 1
    },
    photo: {
        alignItems: 'center',
        marginTop: 26,
        marginBottom: 16
    },
    borderPhoto: {
        borderWidth: 1,
        borderColor: '#8D92A3',
        width: 110,
        height: 110,
        borderRadius: 110,
        borderStyle: 'dashed',
        justifyContent: 'center',
        alignItems: 'center'
    },
    photoContainer: {
        width: 90,
        height: 90,
        borderRadius: 90,
        backgroundColor: '#F0F0F0',
        padding: 24,
    },
    addPhoto: {
        fontSize: 14,
        fontFamily: 'Poppins-Light',
        color: '#8D92A3',
        textAlign: 'center'
    }
})
