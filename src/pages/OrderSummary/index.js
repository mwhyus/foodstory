import React from 'react'
import { StyleSheet, Text, View, SafeAreaView } from 'react-native'
import { DMFood1 } from '../../assets'
import { Button, Gap, Header, ItemListFood, ItemValue } from '../../components'

const OrderSummary = ({ navigation }) => {
    return (
        
        <SafeAreaView>
            <Header title='Payment' subtitle='Check your order' onBack onPress={() => navigation.goBack()} />
            <View>
                <Gap height={24} />
                <View style={styles.content}>
                    <Text>Item Ordered</Text>
                    <ItemListFood type='order-summary' image={DMFood1} items={3} title='Chicken and Couliflower' price={120000} />
                    <Text style={styles.label}>Details Transaction</Text>
                    <ItemValue label='Chicken and Couliflower' value={360000} />
                    <ItemValue label='Driver' value='IDR 50.000' />
                    <ItemValue label='Tax 10%' value='IDR 20.000' />
                    <ItemValue label='Grand Total' value='IDR 430.000' valueColor='#1ABC9C' />
                </View>
                <Gap height={24} />
                <View style={styles.content}>
                    <Text style={styles.label}>Deliver to:</Text>
                    <ItemValue label='Name' value='Saske' />
                    <ItemValue label='Phone No.' value='08123456780' />
                    <ItemValue label='Address' value='Menteng' />
                    <ItemValue label='House No.' value='A20-21' />
                    <ItemValue label='City' value='Jakarta Pusat' />
                </View>
            </View>
            <Gap height={24} />
            <View style={styles.checkout}>
                <Button label='Checkout Now' onPress={() => navigation.replace('OrderSuccess')} />
            </View>
        </SafeAreaView>
    )
}

export default OrderSummary

const styles = StyleSheet.create({
    content: {
        backgroundColor: 'white',
        paddingHorizontal: 24,
        paddingVertical: 16,
    },
    label: {
        fontSize: 14,
        fontFamily: 'Poppins-Regular',
        color: '#020202',
        marginBottom: 8
    },
    checkout: {
        paddingHorizontal: 24,
        
    }
})