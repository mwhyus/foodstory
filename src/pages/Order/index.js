import React, { useState } from 'react'
import { StyleSheet, Text, View, SafeAreaView } from 'react-native'
import { EmptyOrder, Gap, Header, OrderTabSection } from '../../components'

const Order = () => {
    const [isEmpty] = useState(false);
    
    return (
        <SafeAreaView style={styles.container}>
            {isEmpty ? <EmptyOrder /> : (
            <View style={styles.content}>
                <Header title='Your Orders' subtitle='Wait for the best meal' />
                <Gap height={24} />
                <View style={styles.tabSection}>
                    <OrderTabSection />
                </View>
            </View>
            )}
        </SafeAreaView>
    )
}

export default Order

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
    },
    tabSection: {
        flex: 1,
    },
})
