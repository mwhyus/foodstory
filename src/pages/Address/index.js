import React from 'react';
import { ScrollView, StyleSheet, View, SafeAreaView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Gap, Header, Input, Select } from '../../components';
import { useForm } from '../../utils';


const Address = ({ navigation }) => {
    const [form, setForm] = useForm({
        phoneNumber: '',
        address: '',
        houseNumber: '',
        city: 'Makassar',
    })

    const dispatch = useDispatch()
    const registerReducer = useSelector(state => state.registerReducer)

    const onSubmit = () => {
        // console.log('Form addressnya: ', form);
        // const data = {
        //     ...form,
        //     ...registerReducer
        // }
        // console.log('Data addressnya: ', data);
        navigation.replace('SuccessRegister')
    }

    return (
        <SafeAreaView>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
            <View style={styles.bigContainer}>
                <Header
                    onPress={() => navigation.goBack()}
                    title='Address'
                    subtitle='Make sure it is valid'
                    onBack
                />
                <View style={styles.container}>
                    <Input
                        label='Phone Number'
                        placeholder='Enter your phone number'
                        value={form.phoneNumber}
                        onChangeText={(value) => setForm('phoneNumber', value)}
                    />
                    <Gap height={16} />
                    <Input
                        label='Address'
                        placeholder='Enter your address'
                        value={form.address}
                        onChangeText={(value) => setForm('address', value)}
                    />
                    <Gap height={16} />
                    <Input
                        label='House Number'
                        placeholder='Enter your house number'
                        value={form.houseNumber}
                        onChangeText={(value) => setForm('houseNumber', value)}
                    />
                    <Gap height={16} />
                    <Select
                        label='City'
                        value={form.city}
                        onSelectChange={(value) => setForm('city', value)}
                    />
                    <Gap height={16} />
                    <Button label='Register Now' onPress={onSubmit} />
                    <Gap height={10} />
                </View>
            </View>
        </ScrollView>
        </SafeAreaView>
    )
}

export default Address

const styles = StyleSheet.create({
    bigContainer: {
        flex: 1,
    },
    container: {
        backgroundColor: 'white',
        paddingTop: 26,
        paddingHorizontal: 24,
        marginTop: 24,
        flex: 1
    }
})
