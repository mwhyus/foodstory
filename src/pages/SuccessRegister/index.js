import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { ILCompleteRegister } from '../../assets/illustration'
import { Button, Gap } from '../../components'

const SuccessRegister = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Image source={ILCompleteRegister} style={styles.ilustration} />
            <Gap height={30} />
            <Text style={styles.title}>Yeay, Complete!</Text>
            <Gap height={6} />
            <Text style={styles.subTitle}>Now you can find</Text>
            <Text style={styles.subTitle}>Your best story with us</Text>
            <Gap height={30} />
            <View style={styles.buttonContainer}>
                <Button label='Find your story' onPress={() => navigation.replace('MainApp')} />
            </View>
        </View>
    )
}

export default SuccessRegister

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'

    },
    ilustration: {
        height: 250,
        width: 250,

    },
    title: {
        fontSize: 20,
        fontFamily: 'Poppins-Regular',
        color: '#020202'
    },
    subTitle: {
        fontSize: 14,
        fontFamily: 'Poppins-Light',
        color: '#8D92A3'
    },
    buttonContainer: {
        width: '100%',
        paddingHorizontal: 80
    }
})
