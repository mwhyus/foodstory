import { ImageBackground, StyleSheet, Text, TouchableOpacity, View, SafeAreaView } from 'react-native'
import React from 'react'
import { DMFood1, ICBackWhite } from '../../assets'
import { Counter, Rating } from '../../components/molecules'
import { Button, Gap } from '../../components/atoms'

const FoodDetail = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground source={DMFood1} style={styles.cover}>
        <TouchableOpacity style={styles.back} activeOpacity={0.7} onPress={() => navigation.goBack()} >
          <ICBackWhite />
        </TouchableOpacity>
      </ImageBackground>
      <View style={styles.content}>
        <View style={styles.mainContent}>
          <View style={styles.productContainer}>
            <View>
              <Text style={styles.title}>Chicken and Couliflower</Text>
              <Rating />
            </View>
            <Counter />
          </View>
          <Gap height={16} />
          <Text style={styles.description}>
            Juicy chicken thighs and cauliflower in a
            savory cream sauce make for a delicious food that’s perfect
            for when you need a quick, yet hearty meal!
          </Text>
          <View style={styles.subMain}>
            <Gap height={16} />
            <Text style={styles.label}>Ingredients:</Text>
            <Gap height={4} />
            <Text style={styles.description}>Cauliflower, Chicken Thighs, Salt, Pepper</Text>
          </View>
        </View>
        <View style={styles.footer}>
          <View style={styles.priceContainer}>
            <Text style={styles.labelTotal}>Total Price:</Text>
            <Text style={styles.price}>IDR 120.000</Text>
          </View>
          <View style={styles.button}>
            <Button label='Order Now' onPress={() => navigation.navigate('OrderSummary')} />
          </View>
        </View>
      </View>
    </SafeAreaView>
  )
}

export default FoodDetail

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cover: {
    height: 330,
    paddingTop: 26,
    paddingLeft: 22
  },
  back: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainContent: {
    flex: 1,
  },
  subMain: {
    justifyContent: 'flex-end',
    flex: 1},
  content: {
    flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    marginTop: -100,
    paddingTop: 26,
    paddingHorizontal: 16
  },
  productContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
  },
  description: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#8D92A3',
  },
  label: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
  },
  footer: {
    flexDirection: 'row',
    paddingVertical: 16,
    alignItems: 'center',
  },
  priceContainer: {
    flex: 1
  },
  button: {
    width: 163
  },
  labelTotal: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    color: '#8D92A3',
  },
  price: {
    fontSize: 18,
    fontFamily: 'Poppins-Regular',
    color: '#020202',
  }
})