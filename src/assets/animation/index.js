import ANFoodStory from './foodstory.json';
import ANsuccess from './success.json';
import ANsuccess2 from './success2.json';
import ANLookingForFood from './lookingforfood.json';

export {
    ANFoodStory,
    ANsuccess,
    ANsuccess2,
    ANLookingForFood
}