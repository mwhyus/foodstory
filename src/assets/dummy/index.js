import DMPerson from './person.png';
import DMFood1 from './food1.jpg';
import DMFood2 from './food2.jpg';
import DMFood3 from './food3.jpg';
import DMFood4 from './food4.jpg';
import DMFood5 from './Food5.png';

export {
    DMPerson,
    DMFood1,
    DMFood2,
    DMFood3,
    DMFood4,
    DMFood5
}