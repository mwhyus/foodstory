import ICLogo from './logo.svg';
import ICBack from './arrow_back'
import ICNext from './arrow_next'
import ICBackWhite from './ic-back-white.svg'
import ICHomeOn from './ic-home-on.svg'
import ICHomeOff from './ic-home-off.svg'
import ICUserOn from './ic-user-on.svg'
import ICUserOff from './ic-user-off.svg'
import ICOrderOn from './ic-order-on.svg'
import ICOrderOff from './ic-order-off.svg'
import ICStarOn from './ic-star-on.svg'
import ICStarOff from './ic-star-off.svg'
import ICPlus from './ic-plus.svg'
import ICMinus from './ic-minus.svg'

export {
    ICLogo,
    ICBack,
    ICHomeOff,
    ICHomeOn,
    ICUserOff,
    ICUserOn,
    ICOrderOff,
    ICOrderOn,
    ICStarOff,
    ICStarOn,
    ICBackWhite,
    ICPlus,
    ICMinus,
    ICNext
}