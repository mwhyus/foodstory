import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
    Address,
    Home,
    Login,
    Order,
    Profile,
    Register,
    Splash,
    SuccessRegister,
    FoodDetail,
    OrderSummary,
    OrderSuccess,
    OrderDetail,
} from '../pages';
import { BottomNavigator } from '../components';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator()

const MainApp = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen name='Home' component={Home} />
            <Tab.Screen name='Order' component={Order} />
            <Tab.Screen name='Profile' component={Profile} />
        </Tab.Navigator>
    )
}

const Router = () => {
    return (
        <Stack.Navigator headerMode={false}>
            <Stack.Screen name="Splash" component={Splash} />
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Address" component={Address} />
            <Stack.Screen name="SuccessRegister" component={SuccessRegister} />
            <Stack.Screen name="MainApp" component={MainApp} />
            <Stack.Screen name="FoodDetail" component={FoodDetail} />
            <Stack.Screen name="OrderSummary" component={OrderSummary} />
            <Stack.Screen name="OrderSuccess" component={OrderSuccess} />
            <Stack.Screen name="OrderDetail" component={OrderDetail} />

        </Stack.Navigator>
    )
}

export default Router;
